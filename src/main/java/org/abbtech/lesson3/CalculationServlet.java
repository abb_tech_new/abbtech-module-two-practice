package org.abbtech.lesson3;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.abbtech.lesson3.calculator.ApplicationService;
import org.abbtech.lesson3.calculator.CalculatorServiceImpl;
import org.abbtech.lesson4.CalculationRepository;
import org.abbtech.lesson4.JDBCConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

@WebServlet(name = "CalculationServlet", urlPatterns = {"/calculator/add", "/calculator/multiply"})
public class CalculationServlet extends HttpServlet {
    private ApplicationService applicationService;
    private CalculationRepository calculationRepository;

    @Override
    public void init() throws ServletException {
        System.out.println("init worked");
        applicationService = new ApplicationService(new CalculatorServiceImpl());
        calculationRepository = new CalculationRepository();
    }

//burda bezen  service ve destroy metodlari aktiv oldugunda get elediyimde
//    get metodu calismir ona gore comente aldim

//    @Override
//    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        System.out.println("services worked");
//    }
//
//    @Override
//    public void destroy() {
//        System.out.println("destroy worked");
//    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String calculationMethod = req.getHeader("x-calculation-method");
        PrintWriter writer = resp.getWriter();
        int a;
        int b;
        try {
            a = Integer.parseInt(req.getParameter("a"));
            b = Integer.parseInt(req.getParameter("b"));
        } catch (NumberFormatException e) {
            a = 0;
            b = 0;
        }
        int result = 0;
        try {
            switch (calculationMethod) {
                case "multiply" -> result = applicationService.multiply(a, b);
                case "devision" -> result = applicationService.devision(a, b);
                case "addition" -> result = applicationService.addition(a, b);
                case "subtract" -> result = applicationService.subtract(a, b);
            }
            System.out.println(result);
            calculationRepository.saveCalculation(a, b, result, calculationMethod);
            var calculations = calculationRepository.getReadCalculation();
            ;
            writer.write("""
                                    
                    {
                    "result":""" + calculations + """
                    }
                                    
                    """);
        } catch (ArithmeticException arithmeticException) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("invalid exception");
        }
        resp.setContentType("application/json");
        writer.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader bufferedReader = req.getReader();
        PrintWriter writer = resp.getWriter();
        int lineline;
        while ((lineline = bufferedReader.read()) != -1) {
            char chr = (char) lineline;
            writer.write(chr);
        }
        writer.close();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        int id = 0;
        try {
            id = Integer.parseInt(req.getParameter("id"));
        } catch (NumberFormatException e) {
            writer.write("""
                                    
                    {
                    "result":""" + "please correct id number" + """
                    }
                                    
                    """);
        }
        calculationRepository.deleteCalculation(id);
        writer.write("""
                                
                {
                "result":""" + "ok" + """
                }
                                
                """);
        resp.setContentType("application/json");
        writer.close();
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        int a;
        int b;
        int id;

        try {
            a = Integer.parseInt(req.getParameter("a"));
            b = Integer.parseInt(req.getParameter("b"));
            id = Integer.parseInt(req.getParameter("id"));
        } catch (NumberFormatException e) {
            a = 0;
            b = 0;
            id = 0;
        }

        try {
            calculationRepository.patchCalculation(a, b, id);
            writer.write("""
                                    
                    {
                    "result":""" + "ok" + """
                    }
                                    
                    """);
        } catch (ArithmeticException arithmeticException) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("invalid exception");
        }
        resp.setContentType("application/json");
        writer.close();
    }
}
