package org.abbtech.lesson3.calculator;

public interface CalculatorService {
    int multiply(int a , int b);
    int subtract(int a , int b);
    int addition(int a , int b);
    int devision (int a , int b);
}
