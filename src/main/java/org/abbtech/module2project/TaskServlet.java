package org.abbtech.module2project;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "TaskServlet", urlPatterns = "/tasks")
public class TaskServlet extends HttpServlet {
    private TaskRepositery taskRepositery;

    @Override
    public void init() throws ServletException {
        System.out.println("init");
        taskRepositery = new TaskRepositery();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            taskRepositery.getTask();
        } catch (Exception e) {
            throw new ServletException("Error retrieving tasks", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String description = req.getParameter("desc");
            Boolean completed = Boolean.valueOf(req.getParameter("comp"));
            int userId = Integer.parseInt(req.getParameter("id"));
            taskRepositery.postTask(description, completed, userId);
            resp.setStatus(HttpServletResponse.SC_CREATED);
            resp.getWriter().write("Task created successfully.");
        } catch (NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("Invalid input data.");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int taskId = Integer.parseInt(req.getParameter("taskId"));
            taskRepositery.deleteTask(taskId);
            resp.getWriter().write("Task deleted successfully.");
        } catch (NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("Invalid task ID.");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int taskId = Integer.parseInt(req.getParameter("taskId"));
            String description = req.getParameter("desc");
            Boolean completed = Boolean.valueOf(req.getParameter("comp"));
            int userId = Integer.parseInt(req.getParameter("userId"));
            taskRepositery.updateTask(description, completed, userId, taskId);
            resp.getWriter().write("Task updated successfully.");
        } catch (NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("Invalid input data.");
        }
    }
}
