package org.abbtech.module2project;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "UserServlet", urlPatterns = "/users")
public class UserServlet extends HttpServlet {
    private UserRepositery userRepositery;

    @Override
    public void init() throws ServletException {
        System.out.println("init users");
        userRepositery = new UserRepositery();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            userRepositery.getUser();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String password = req.getParameter("pass");
        PrintWriter writer = resp.getWriter();
        if (name == null || name.isEmpty() || password == null || password.isEmpty()) {
            writer.write("Please provide both name and password.");
        } else {
            userRepositery.createUser(name, password);
            writer.write("User created successfully.");
        }

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("put");
        String name = req.getParameter("name");
        String password = req.getParameter("pass");
        int userId = 0;
        try {
            userId = Integer.parseInt(req.getParameter("id"));
        } catch (NumberFormatException e) {
            System.err.println("Invalid user id: " + req.getParameter("id"));
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        try {
            userRepositery.updateUser(userId, name, password);
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.getWriter().write("User updated successfully.");
        } catch (Exception e) {
            System.err.println("Error updating user: " + e.getMessage());
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            resp.getWriter().write("Error updating user.");
        }
    }


    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("delete");
        int id = 0;
        try {
            id = Integer.parseInt(req.getParameter("id"));
        } catch (NumberFormatException e) {
        }

        PrintWriter writer = resp.getWriter();
        if (id <= 0) {
            writer.write("Please provide a valid delete id.");
        } else {
            userRepositery.deleteUser(id);
            writer.write("User deleted successfully.");
        }
    }

}
