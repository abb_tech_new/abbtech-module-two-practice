package org.abbtech.module2project;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TaskRepositery {
    private static final String JDBC_URL = "jdbc:postgresql://127.0.0.1:5432/abb_tech";
    private static final String JDBC_USERNAME = "mehman";
    private static final String PASS = "pass";

    public void postTask(String description, Boolean comp, int userId) {
        Connection connection = null;
        String sqlInsert = """
                insert into "tasks".task( description, completed, userid) VALUES (?,?,?);
                    """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
            preparedStatement.setString(1, description);
            preparedStatement.setString(2, String.valueOf(comp));
            preparedStatement.setInt(3, userId);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteTask(int id) {
        Connection connection = null;
        String sqlDelete = """
                delete from "tasks".task where id=?;
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }

    public void updateTask(String description, Boolean completed, int userId, int id) {
        Connection connection = null;
        String sqlUpdate = """
                update "tasks".task set  description=? ,completed=? ,userid=? where id=?
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setString(1, description);
            preparedStatement.setString(2, String.valueOf(completed));
            preparedStatement.setInt(3, userId);
            preparedStatement.setInt(4, id);
            preparedStatement.execute();
            connection.commit();

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }

    public List<TaskDTO> getTask() {
        Connection connection = null;
        List<TaskDTO> task = new ArrayList<>();
        String sqlAll = """
                select  * from "tasks".task
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, PASS);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlAll);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String descrtiption = resultSet.getString("descrtiption");
                Boolean comploted = resultSet.getBoolean("comploted");
                int userId = resultSet.getInt("userId");
                int id = resultSet.getInt("id");
                task.add(new TaskDTO(id, userId, descrtiption, comploted));
            }
            return task;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }
}

