package org.abbtech.lesson1.task;

public class Main {
    public static void main(String[] args) {

        Calculator calculator = new Calculator();
        var addition =  calculator.addition(10,10);
        System.err.println("result addition : " + addition);
        var subtraction =  calculator.subtraction(20,10);
        System.err.println("result subtraction : " + subtraction);
        var multiplication =  Calculator.multiplication(20,10);
        System.err.println("result multiplication : " + multiplication);
        var division =  Calculator.division(20,10);
        System.err.println("result division : " + division);

    }
}
