package org.abbtech.lesson1;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        var a = calculator.addition(10, 10);
        var b = Calculator.multiplication(10, 10);
        System.out.println(" addition result " + a);
        System.out.println(" multiplication result " + b);
    }
}
