package org.abbtech.lesson4;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CalculationRepository {

    private static final String JDBC_URL = "jdbc:postgresql://127.0.0.1:5432/abb_tech";
    private static final String JDBC_USERNAME = "mehman";
    private static final String PASS = "pass";

    public void deleteCalculation(int id) {
        Connection connection = null;
        String sqlDelete = """
                delete from "calculation".calculation_result where id=?;    
                 """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void patchCalculation(int a, int b, int id) {
        Connection connection = null;
        String sqlUpdate = """
                update  "calculation".calculation_result set variable_a=?,variable_b=? where id=?;    
                 """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setInt(1, a);
            preparedStatement.setInt(2, b);
            preparedStatement.setInt(3, id);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void saveCalculation(int a, int b, int result, String method) {
        Connection connection = null;
        String sqlInsert = """
                insert into "calculation".calculation_result(variable_a, variable_b, calc_result, calc_method) 
                VALUES (?,?,?,?)
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
            preparedStatement.setInt(1, a);
            preparedStatement.setInt(2, b);
            preparedStatement.setInt(3, result);
            preparedStatement.setString(4, method);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }


    public List<CalculationResultDTO> getReadCalculation() {
        Connection connection = null;
        List<CalculationResultDTO> calculations = new ArrayList<>();
        String sqlAll = """
                select * from "calculation".calculation_result ;    
                 """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, PASS);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlAll);
            ResultSet calculationResult = preparedStatement.executeQuery();
            while (calculationResult.next()) {
                int a = calculationResult.getInt("variable_a");
                int b = calculationResult.getInt("variable_b");
                int result = calculationResult.getInt("calc_result");
                String method = calculationResult.getString("calc_method");
                calculations.add(new CalculationResultDTO(a, b, result, method));
            }
            return calculations;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
