package org.abbtech.lesson4;

public record CalculationResultDTO(int a, int b, int result, String method) {
}
