package org.abbtech.lesson2;

public class UserServiceImpl implements UserServices {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean isUserActive(String username) {
        User user = userRepository.findByUsername(username);
        return user != null && user.isActive();
    }

    public void deleteUser(int userId) throws Exception {
        User user = userRepository.findUserId((long) userId);
        if (user == null) {
            throw new Exception();
        }
    }

    public User getUser(int userId) throws Exception {
        User user = userRepository.findUserId((long) userId);
        if (user == null) {
            throw new Exception();
        }
        return user;
    }


    ;
}
