package org.abbtech.lesson2;

public interface UserRepository {
    User findByUsername(String userName);
    User findUserId(Long userId);
    void delete(Long userId);
}
