package org.abbtech.lesson1.task;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {

    Calculator calculator;

    @BeforeEach
    void init() {
        calculator = new Calculator();
    }

    @Test
    void additionTest() {
        assertEquals(20, calculator.addition(10, 10));
    }

    @Test
    void multiplicationTest() {
        assertTrue(Calculator.multiplication(5, 3) == 15);
    }

    @Test
    void divisionTest() {
        assertFalse(Calculator.division(10, 2) == 3);
    }

    @Test
    void subtractionTest() {
        assertNull(calculator.subtraction(20, 10));
    }

    @Test
    void nullTest() {
        assertNotNull(calculator.addition(5, 5));
    }

    @Test
    void assertArrayEqualsTest() {
        int[] expectedArray = {1, 2, 3};
        int[] resultArray = {1, 2, 3};
        assertArrayEquals(expectedArray, resultArray);
    }

    @Test
    void assertSameTest() {
        String string1 = "Java";
        String string2 = "Java";
        assertSame(string1, string2);
    }

    @Test
    void assertNotSameTest() {
        String string1 = "Java";
        String string3 = new String("Java");
        assertNotSame(string1, string3);
    }

}
