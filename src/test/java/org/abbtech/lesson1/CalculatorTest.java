package org.abbtech.lesson1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculatorTest {
    Calculator calculator;

    @BeforeEach
    void init() {
        calculator = new Calculator();
    }

    @Test
    void additionTest() {
        int actualResult = calculator.addition(10, 10);
        Assertions.assertEquals(actualResult, 20);
    }

    @Test
    void multiplicationTest() {
        int actualResult = Calculator.multiplication(10, 10);
        Assertions.assertEquals(actualResult, 100);
    }
}
