//package org.abbtech.lesson3;
//
//import jakarta.servlet.ServletException;
//import jakarta.servlet.http.*;
//import jakarta.servlet.http.*;
//import org.abbtech.lesson3.CalculationServlet;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.io.StringWriter;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.when;
//
//public class CalculationServletUnitTest {
//
//    private CalculationServlet servlet;
//    private HttpServletRequest request;
//    private HttpServletResponse response;
//    private StringWriter stringWriter;
//
//    @BeforeEach
//    void setUp() {
//        servlet = new CalculationServlet();
//        request = Mockito.mock(HttpServletRequest.class);
//        response = Mockito.mock(HttpServletResponse.class);
//
//        stringWriter = new StringWriter();
//        PrintWriter printWriter = new PrintWriter(stringWriter);
//        try {
//            when(response.getWriter()).thenReturn(printWriter);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    void testDoGet_multiply() throws IOException, ServletException {
//        when(request.getHeader("x-calculation-method")).thenReturn("multiply");
//        when(request.getParameter("a")).thenReturn("5");
//        when(request.getParameter("b")).thenReturn("3");
//
//        servlet.doGet(request, response);
//
//        String expected = "{\n" +
//                "    \"result\":15\n" +
//                "}\n";
//        assertEquals(expected, stringWriter.toString());
//    }
//
//    @Test
//    void testDoGet_division() throws IOException, ServletException {
//        when(request.getHeader("x-calculation-method")).thenReturn("divide");
//        when(request.getParameter("a")).thenReturn("10");
//        when(request.getParameter("b")).thenReturn("2");
//
//        servlet.doGet(request, response);
//
//        String expected = "{\n" +
//                "    \"result\":5\n" +
//                "}\n";
//        assertEquals(expected, stringWriter.toString());
//    }
//
//    @Test
//    void testDoGet_invalidCalculationMethod() throws IOException, ServletException {
//        when(request.getHeader("x-calculation-method")).thenReturn("invalid");
//        when(request.getParameter("a")).thenReturn("5");
//        when(request.getParameter("b")).thenReturn("3");
//
//        servlet.doGet(request, response);
//
//        assertEquals("invalid exception", stringWriter.toString());
//    }
//
//    @Test
//    void testDoGet_invalidNumberFormat() throws IOException, ServletException {
//        when(request.getHeader("x-calculation-method")).thenReturn("multiply");
//        when(request.getParameter("a")).thenReturn("abc");
//        when(request.getParameter("b")).thenReturn("def");
//
//        servlet.doGet(request, response);
//
//        String expected = "{\n" +
//                "    \"result\":0\n" +
//                "}\n";
//        assertEquals(expected, stringWriter.toString());
//    }
//}
