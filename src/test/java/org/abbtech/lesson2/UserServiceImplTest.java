package org.abbtech.lesson2;

import org.abbtech.lesson2.User;
import org.abbtech.lesson2.UserRepository;
import org.abbtech.lesson2.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    void isUserActive_shouldReturnTrueWhenUserIsActive() {
        String username = "Mehman";
        User activeUser = new User(1L, "Mehman", 22, true);
        when(userRepository.findByUsername(username)).thenReturn(activeUser);

        assertTrue(userService.isUserActive(username));
    }

    @Test
    void deleteUser_shouldCallDeleteMethodOnRepositoryWhenUserFound() throws Exception {
        Long userId = 1L;
        User user = new User(userId, "Mehman", 22, true);
        when(userRepository.findUserId(userId)).thenReturn(user);

        userService.deleteUser(Math.toIntExact(userId));

        verify(userRepository).delete(userId);
    }

    @Test
    void deleteUser_shouldThrowExceptionWhenUserNotFound() {
        Long userId = 1L;
        when(userRepository.findUserId(userId)).thenReturn(null);
        assertThrows(Exception.class, () -> userService.deleteUser(Math.toIntExact(userId)));
    }

    @Test
    void getUser_shouldReturnUserWhenFound() throws Exception {
        Long userId = 1L;
        User user = new User(userId, "Mehman", 25, true);
        when(userRepository.findUserId(userId)).thenReturn(user);

        User retrievedUser = userService.getUser(Math.toIntExact(userId));

        assertSame(user, retrievedUser);
    }

    @Test
    void getUser_shouldThrowExceptionWhenUserNotFound() {
        Long userId = 1L;
        when(userRepository.findUserId(userId)).thenReturn(null);
        assertThrows(Exception.class, () -> userService.getUser(Math.toIntExact(userId)));
    }
}
